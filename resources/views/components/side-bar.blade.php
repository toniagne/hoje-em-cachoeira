<div class="side-bar-block filter-block">
    <h3>Filtros</h3>
    <p>Faça uma pesquisa mais detalhada</p>

    <div class="panels-group">

        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="#panel-1" data-toggle="collapse" >Categorias<span><i class="fa fa-angle-down"></i></span></a>
            </div><!-- end panel-heading -->

            <div id="panel-1" class="panel-collapse collapse">
                <div class="panel-body text-left">
                    <ul class="list-unstyled">
                        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox"/>
                            <label for="check01"><span><i class="fa fa-check"></i></span>Automoveis</label></li>
                        <li class="custom-check"><input type="checkbox" id="check02" name="checkbox"/>
                            <label for="check02"><span><i class="fa fa-check"></i></span>Aruba Mecanicas</label></li>
                        <li class="custom-check"><input type="checkbox" id="check03" name="checkbox"/>
                            <label for="check03"><span><i class="fa fa-check"></i></span>Lojas</label></li>

                    </ul>
                </div><!-- end panel-body -->
            </div><!-- end panel-collapse -->
        </div><!-- end panel-default -->

        <button id="panel-1" name="checkbox">Pesquisar</button> <!--fazer o botao pesquisar!!!-->

    </div><!-- end panel-group -->


</div><!-- end side-bar-block -->

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-12">
        <div class="side-bar-block main-block ad-block">
            <div class="main-img ad-img">
                <a href="#">
                    <img src="{{asset('images/car-ad.jpg')}}" class="img-responsive" alt="car-ad" />
                    <div class="ad-mask">
                        <div class="ad-text">
                            <span>Banner</span>
                            <h2>Destaque</h2>
                            <span>Empresa</span>
                        </div><!-- end ad-text -->
                    </div><!-- end columns -->
                </a>
            </div><!-- end ad-img -->
        </div><!-- end side-bar-block -->
    </div><!-- end columns -->

    <div class="col-xs-12 col-sm-6 col-md-12">
        <div class="side-bar-block support-block">
            <h3>Sua empresa em destaque</h3>
            <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
            <div class="support-contact">
                <span><i class="fa fa-phone"></i></span>
                <p>+1 123 1234567</p>
            </div><!-- end support-contact -->
        </div><!-- end side-bar-block -->
    </div><!-- end columns -->
</div><!-- end row -->
