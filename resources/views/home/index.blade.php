@extends('layouts.site.app')
@section('title', 'Seja Bem Vindo')
@section('content')
@include('components.mobile-menu')

<!--========================= FLEX SLIDER =====================-->
<section class="flexslider-container" id="flexslider-container-1">

    <div class="flexslider slider" id="slider-1">
        <ul class="slides">

            <li class="item-1" style="background:linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url({{ asset('storage/public/'.$configs->slides)}}) 50% 0%;
	background-size:cover;
	height:100%;">
                <div class=" meta">
                    <div class="container">

                    </div><!-- end container -->
                </div><!-- end meta -->
            </li><!-- end item-1 -->



        </ul>
    </div><!-- end slider -->

    <div class="search-tabs" id="search-tabs-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">



                    <div class="tab-content d-flex ">

                        <div id="flights" class="tab-pane in active">
                            <form method="POST" action="{{ route('home.search') }}">
                                @csrf
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                        <div class="row">

                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group left-icon">
                                                    <input type="text" name="words" class="form-control" placeholder="Pesquisa" >
                                                    <i class="fa fa-search"></i>
                                                </div>
                                            </div><!-- end columns -->

                                        </div><!-- end row -->
                                    </div><!-- end columns -->



                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 search-btn">
                                        <button class="btn btn-orange">Pesquisa</button>
                                    </div><!-- end columns -->

                                </div><!-- end row -->
                            </form>
                        </div><!-- end tours -->


                    </div><!-- end tab-content -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end search-tabs -->

</section><!-- end flexslider-container -->

<div id="team" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="owl-carousel owl-theme" id="owl-team">

                    @foreach($categories as $category)
                    <div class="item owl-item" >
                        <div class="member-block" >
                            <div class="member-img">
                                <a href="{{ route('home.category', $category->slug) }}">
                                    <img src="{{ asset('storage/public/'.$category->icon) }}" class="img-responsive img-circle" alt="member-img"/>
                                </a>
                            </div><!-- end member-img -->

                            <div class="member-name">
                                <a href="{{ route('home.category', $category->slug) }}">
                                    <h3>{{ $category->name }}</h3>
                                </a>
                            </div><!-- end member-name -->
                        </div><!-- end member-block -->
                    </div><!-- end item -->
                    @endforeach

                </div><!-- end owl-team -->
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end team -->

</div><!-- end about-us -->
</section><!-- end innerpage-wrapper -->

<!--=============== EMPRESAS EM DESTAQUE ===============-->
<section id="hotel-offers" class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-heading">
                    <h2>O QUE HÁ DE NOVO</h2>
                    <hr class="heading-line" />
                </div><!-- end page-heading -->

                <div class="owl-carousel owl-theme owl-custom-arrow" id="owl-hotel-offers">

                    @foreach($clients_list as $featured)
                    <div class="item">
                        <div class="main-block hotel-block">
                            <div class="main-img">
                                <a href="{{ route('home.details', $featured->client->slug) }}">
                                    <img src="{{asset('storage/public/'.$featured->file)}}" class="img-responsive"  />
                                </a>
                                <div class="main-mask">
                                    <ul class="list-unstyled list-inline offer-price-1">
                                        <li class="price"><i class="fa fa-whatsapp"></i>{{ $featured->client->phone }}</li>
                                        <li class="rating">
                                            <span><i class="fa fa-star orange"></i></span>
                                            <span><i class="fa fa-star orange"></i></span>
                                            <span><i class="fa fa-star orange"></i></span>
                                            <span><i class="fa fa-star orange"></i></span>
                                            <span><i class="fa fa-star lightgrey"></i></span>
                                        </li>
                                    </ul>
                                </div><!-- end main-mask -->
                            </div><!-- end offer-img -->

                            <div class="main-info hotel-info">
                                <div class="arrow">
                                    <a href="{{ route('home.details', $featured->client->slug) }}"><span><i class="fa fa-angle-right"></i></span></a>
                                </div><!-- end arrow -->

                                <div class="main-title hotel-title">
                                    <a href="#">{{ $featured->client->name }}</a>
                                    <p>{{ $featured->category->name }}</p>
                                </div><!-- end hotel-title -->
                            </div><!-- end hotel-info -->
                        </div><!-- end hotel-block -->
                    </div><!-- end item -->
                    @endforeach
                </div><!-- end owl-hotel-offers -->

                <div class="view-all text-center">
                    <a href="#" class="btn btn-orange">Ver todas</a>
                    <BR><bR>
                </div><!-- end view-all -->
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end hotel-offers -->
@endsection
