<!--======================= FOOTER =======================-->
<section id="footer" class="ftr-heading-o ftr-heading-mgn-1">

    <div id="footer-top" class="banner-padding ftr-top-grey ftr-text-white">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-contact">
                    <h3 class="footer-heading">CONTATOS</h3>
                    <ul class="list-unstyled">
                        <!--<li><span><i class="fa fa-map-marker"></i></span></li>-->
                        <li><span><i class="fa fa-phone"></i></span>(51) 99178-0304</li>
                        <li><span><i class="fa fa-envelope"></i></span><font size=2>faleconosco@hojeemcachoeira.com.br</font></li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 footer-widget ftr-links">
                    <h3 class="footer-heading">CATEGORIAS</h3>
                    <ul class="list-unstyled">
                        @foreach ($categories as $category)
                            <li><a href="{{ route('home.category', $category->slug) }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>



                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-links ftr-pad-left">
                    <h3 class="footer-heading"></h3>
                    <ul class="list-unstyled">
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer-widget ftr-about">
                    <h3 class="footer-heading">QUEM SOMOS</h3>
                    <p>{!! $configs->release !!}
                    <ul class="social-links list-inline list-unstyled">
                        <li><a href="https://pt-br.facebook.com/"><span><i class="fa fa-facebook"></i></span></a></li>
                        <li><a href="https://twitter.com/login?lang=pt"><span><i class="fa fa-twitter"></i></span></a></li>
                        <li><a href="https://accounts.google.com/"><span><i class="fa fa-google-plus"></i></span></a></li>
                        <li><a href="https://br.pinterest.com/"><span><i class="fa fa-pinterest-p"></i></span></a></li>
                        <li><a href="https://www.instagram.com/"><span><i class="fa fa-instagram"></i></span></a></li>
                        <li><a href="https://br.linkedin.com/"><span><i class="fa fa-linkedin"></i></span></a></li>
                        <li><a href="https://www.youtube.com/"><span><i class="fa fa-youtube-play"></i></span></a></li>
                    </ul>
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-top -->

    <div id="footer-bottom" class="ftr-bot-black">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
                    <p>© {{now('Y')->format('Y')}} |  <a href="#">Hoje em Cachoeira</a>. Todos os direitos reservados.</p>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
                    <ul class="list-unstyled list-inline">
                        <li><a href="{{ route('home.terms') }}">Termos de serviço</a></li>
                        <li><a href="{{ route('home.policies') }}">Política de privacidade</a></li>
                    </ul>
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-bottom -->

</section><!-- end footer -->


<!-- Page Scripts Starts -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.flexslider.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/custom-navigation.js')}}"></script>
<script src="{{asset('js/custom-flex.js')}}"></script>
<script src="{{asset('js/custom-owl.js')}}"></script>
<script src="{{asset('js/custom-date-picker.js')}}"></script>
<script src="{{asset('js/custom-video.js')}}"></script>

<!-- Page Scripts Ends -->
@stack('script')
</body>
</html>
