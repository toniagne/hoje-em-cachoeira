@include('layouts.site.header')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ee0485f35f117cb"></script>

<body id="main-homepage">

<!--====== LOADER =====-->
<div class="loader"></div>


<!--======== SEARCH-OVERLAY =========-->


@include('layouts.site.top-bar')

@yield('content')

@include('layouts.site.footer')
