@extends('layouts.admin.app')
@section('title', 'Cadastro')
@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->

    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{asset("plugins/ekko-lightbox/ekko-lightbox.css")}}">
@endpush
@section('content-header')
   Editando o catalogo {{$catalog->name }} do cliente {{ $catalog->client->name }}
@endsection

@section('content')

    <!-- Main content -->
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                @if ($errors->any())
                    <div class="card-body">
                        <div class="callout callout-danger">
                            <h5>Preencha corretamente os campos.</h5>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
            @endif

            <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="{{ route('catalogs.update', $catalog->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Nome da galeria / Título</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $catalog->name }}" placeholder="Título da galeria">
                        </div>

                        <div class="form-group">
                            <label for="name">Nome do cliente</label>
                             <input type="text" id='name' class="form-control" value="{{ $catalog->client->name }}" disabled>
                        </div>

                        <!-- Main content -->
                        <section class="content">
                            <div class="container-fluid">
                                <div class="row">

                                    <div class="col-12">
                                        <div class="card card-primary">

                                            <div class="card-body">
                                                <div class="row">
                                                    @foreach ($catalog->attachments()->get() as $photo)
                                                    <div class="col-sm-2">
                                                        <div style="height: 150px;">
                                                        <a href="{{ asset('storage/public/'.$photo->path) }}" data-toggle="lightbox" data-title="sample 1 - white" data-gallery="gallery">
                                                            <img src="{{ asset('storage/public/'.$photo->path) }}" class="img-fluid mb-2" alt="white sample" width="150px">
                                                        </a>
                                                        </div>
                                                        <input type="text" class="form-control" name="fileDescription[id][]"><br>
                                                        <a href=""><i class="fa fa-trash"></i></a>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.container-fluid -->
                        </section>
                        <!-- /.content -->


                        <br><br>
                  <input type="submit" class="btn btn-block btn-success" value="CADASTRAR">
                </form>


            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    @push('scripts')

            <!-- optionally if you need translation for your language then include  locale file as mentioned below -->
        <!-- Ekko Lightbox -->
            <script src="{{asset("plugins/ekko-lightbox/ekko-lightbox.min.js")}}"></script>
                <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                <!-- piexif.min.js is needed for auto orienting image files OR when restoring exif data in resized images and when you
                    wish to resize images before upload. This must be loaded before fileinput.min.js -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="text/javascript"></script>
                <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
                    This must be loaded before fileinput.min.js -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="text/javascript"></script>
                <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for
                    HTML files. This must be loaded before fileinput.min.js -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/purify.min.js" type="text/javascript"></script>
                <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js
                   3.3.x versions without popper.min.js. -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
                <!-- bootstrap.min.js below is needed if you wish to zoom and preview file content in a detail modal
                    dialog. bootstrap 4.x is supported. You can also use the bootstrap js 3.3.x versions. -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>
                <!-- the main fileinput plugin file -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>

                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/pt-BR.js"></script>

                <script>

                $(function () {

                    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                        event.preventDefault();
                        $(this).ekkoLightbox({
                            alwaysShowClose: true
                        });
                    });

                    $('.filter-container').filterizr({gutterPixels: 3});
                    $('.btn[data-filter]').on('click', function() {
                        $('.btn[data-filter]').removeClass('active');
                        $(this).addClass('active');
                    });
                })
            </script>
        @if ($errors->any())
            {!! toastr()->error('Houve um erro no cadastro', 'Atenção')->render() !!}
        @endif
    @endpush
@endsection

