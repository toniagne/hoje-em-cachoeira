@extends('layouts.admin.app')
@section('title', 'Cadastro')
@push('styles')
    <link rel="stylesheet" href="{{ asset("css/dataTables.bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{asset("plugins/summernote/summernote-bs4.css")}}">
@endpush
@section('content-header')
    Cadastrar novo cliente
@endsection

@section('content')

    <!-- Main content -->
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-primary">

                    @if ($errors->any())
                    <div class="card-body">
                        <div class="callout callout-danger">
                            <h5>Preencha corretamente os campos.</h5>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                <!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="{{ route('clients.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Nome do cliente</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Nome do cliente">
                        </div>
                        <div class="form-group">
                            <label for="name">CNPJ / CPF</label>
                            <input type="text" class="form-control" id="document" name="document" value="{{ old('document') }}" placeholder="CNPJ/CPF">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input type="email" value="{{ old('email') }}" name="email" class="form-control" id="exampleInputEmail1" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefone comercial</label>
                            <input type="text" class="form-control phone" name="phone_comercial" id="phone_comercial" value="{{ old('phone_comercial') }}" placeholder="Telefone comercial">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefone/Whatsapp</label>
                            <input type="text" class="form-control phone" name="phone" id="phone" value="{{ old('phone') }}" placeholder="Telefone/WhatsApp">
                        </div>
                        <div class="form-group">
                            <label for="cep">CEP</label>
                            <input type="text" name="cep"  class="form-control cep" value="{{ old('cep') }}" id="cep" placeholder="CEP">
                        </div>
                        <div class="form-group">
                            <label for="address">Endereço</label>
                            <input type="text" name="address"  class="form-control" value="{{ old('address') }}" id="address" placeholder="Endereço completo">
                        </div>

                        <div class="form-group">
                            <label for="facebook">Perfil Facebook</label>
                            <input type="text" name="facebook"  class="form-control" value="{{ old('facebook') }}" id="facebook" placeholder="Endereço completo">
                        </div>

                        <div class="form-group">
                            <label for="address">Release / Quem Somos</label>
                            <textarea class="textarea" name="release" placeholder="Place some text here"  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('release')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="services">Resumo dos serviços da empresa</label>
                            <textarea class="textarea" name="services" placeholder="Serviços que a empresa presta"  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('services')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">Logotipo</label>
                            <input type="file" name="logo" id="exampleInputFile">

                            <p class="help-block">Arquivos PNG - JPG.</p>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-block btn-success" value="CADASTRAR">
                </form>


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    @push('scripts')
            <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
        <script type="text/javascript">
            $(function () {
                // Summernote
                $('.textarea').summernote()
            })
        </script>
        @if ($errors->any())
            {!! toastr()->error('Houve um erro no cadastro', 'Atenção')->render() !!}
        @endif
    @endpush
@endsection

