<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Client;
use App\Models\Config;
use App\Models\Contract;
use App\Models\Project;
use Illuminate\Http\Request;
use View;

class SiteController extends Controller
{
    protected $configs;
    protected $categories;
    protected $contracts;
    protected $client;

    public function __construct()
    {
        $this->configs = Config::all()->first();
        $this->categories = Category::all();
        $this->contracts = new Contract();
        $this->client = new Client();

        View::share('configs',$this->configs);
        View::share('categories',$this->categories);
    }

    public function index(){

        $clients_list = $this->contracts->isValid();
       return view('home.index', compact('clients_list'));
   }

    public function category($slug){
        $category = Category::where('slug', $slug)->first();
        $clients  = Contract::where('category_id', $category->id)->get();
        return view('home.search', ['words' => $slug, 'results' => $clients]);
    }

   public function search(Request $request){
       $contractSearch = $this->contracts->where('tags', 'like', '%'.$request->words.'%')->get();
       return view('home.search', ['words' => $request->words, 'results' => $contractSearch]);
   }

    public function details($client){
        $clientSelected = $this->client->where('slug', $client)->first();
        return view('home.detail', ['client' => $clientSelected]);
    }

    public function register(){
        return view('home.register');
    }

    public function about(){
        return view('home.about');
    }

    public function terms(){
        return view('home.terms');
    }

    public function policies(){
        return view('home.policies');
    }

    public function projects(){
        $projects = Project::all();
        return view('home.projects', compact('projects'));
    }

    public function projectDetail($slug){
        $project = Project::where('slug', $slug)->first();
        return view('home.project-detail', compact('project'));
    }

}
