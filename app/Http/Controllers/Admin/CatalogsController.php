<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Catalog;
use App\Models\Client;
use Illuminate\Http\Request;

class CatalogsController extends Controller
{

    public function index()
    {
        $catalogs = Catalog::all();
        return view('admin.catalogs.index', compact('catalogs'));
    }


    public function create()
    {
        $clients = Client::all();
        return view('admin.catalogs.create', compact('clients'));
    }


    public function store(Request $request)
    {

       $catalog = Catalog::create($request->all());

       $catalog->sendFiles($request->attachments);

       return redirect(route('catalogs.index'))->with('message', 'Catálogo lançado com sucesso !');
    }


    public function show($id)
    {
        //
    }


    public function edit(Catalog $catalog)
    {
       return view('admin.catalogs.edit', compact('catalog'));


    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Catalog $catalog)
    {
        $catalog->delete();

        return redirect(route('catalogs.index'))->with('message', 'Catálogo apagado com sucesso !');

    }
}
