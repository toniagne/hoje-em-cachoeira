<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'city_id',
        'name',
        'document',
        'email',
        'phone',
        'address',
        'cep',
        'facebook',
        'release',
        'file',
        'active',
        'slug',
        'services'
    ];


    public function contracts()
    {
        return $this->belongsTo(Contract::class, 'id', 'client_id');
    }

    public function catalog(){
        return $this->belongsTo(Catalog::class, 'id', 'client_id');
    }

    public function sendFile($request){
        $path = $request->store('clients');
        return $path;
    }

    public function situation(){
        return $this->active = 1 ? 'Ativo': 'Inativo';
    }
}
