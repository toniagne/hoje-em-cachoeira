<?php

use Illuminate\Support\Facades\Route;

Route::get('commands/{command}', function($command) {
    try {
        \Illuminate\Support\Facades\Artisan::call($command);
    } catch(\Exception $e){
        dd($e->getMessage());
    }

    echo \Illuminate\Support\Facades\Artisan::output();
});

Route::get('image_show/{filename}', 'ImageController@displayImage')->name('image.show');

// ROTAS DO SITE
Route::group(['prefix'=>'/', 'namespace' => 'Home', 'as' => 'home.'], function() {
    Route::get('/', ['uses' => 'SiteController@index'])->name('index');
    Route::post('/busca', ['uses' => 'SiteController@search'])->name('search');
    Route::get('{client}/detalhes/', ['uses' => 'SiteController@details'])->name('details');
    Route::get('projeto/{slug}', ['uses' => 'SiteController@projectDetail'])->name('project.details');
    Route::get('/contato', ['uses' => 'SiteController@contact'])->name('contact');
    Route::get('/quem-somos', ['uses' => 'SiteController@about'])->name('about');
    Route::get('/projetos', ['uses' => 'SiteController@projects'])->name('projects');
    Route::get('/registro', ['uses' => 'SiteController@register'])->name('register');
    Route::get('/categorias', ['uses' => 'SiteController@categories'])->name('categories');
    Route::get('/categorias/{slug}', ['uses' => 'SiteController@category'])->name('category');
    Route::get('politica-de-privacidade', ['uses' => 'SiteController@policies'])->name('policies');
    Route::get('termos-de-uso', ['uses' => 'SiteController@terms'])->name('terms');
});



Auth::routes();

// ROTAS DO DASH ADMIN
Route::group(['prefix'=>'/admin',  'namespace' => 'Admin', 'middleware' => ['auth']], function() {

    Route::get('/', ['as' => 'admin.dash', 'uses' => 'AdminController@index']);

    Route::resource('/clients', 'ClientsController');

    Route::resource('/advertisements', 'AdvertisementsController');

    Route::resource('/users', 'UsersController');

    Route::resource('/payments', 'PaymentsController');

    Route::resource('/categories', 'CategoriesController');

    Route::resource('/contracts', 'ContractsController');

    Route::resource('/catalogs', 'CatalogsController');

    Route::resource('/configs', 'ConfigsController');

    Route::resource('/usefuls', 'UsefulsController');

    Route::resource('/projects', 'ProjectsController');
});


Route::get('/home', 'HomeController@index')->name('home');
